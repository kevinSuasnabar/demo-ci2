package com.boot.bookingrestaurantapi.services;

import com.boot.bookingrestaurantapi.entities.Board;
import com.boot.bookingrestaurantapi.entities.Reservation;
import com.boot.bookingrestaurantapi.entities.Restaurant;
import com.boot.bookingrestaurantapi.entities.Turn;
import com.boot.bookingrestaurantapi.exceptions.BookingException;
import com.boot.bookingrestaurantapi.jsons.RestaurantRest;
import com.boot.bookingrestaurantapi.repositories.RestaurantRepository;
import com.boot.bookingrestaurantapi.services.impl.RestaurantServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RestaurantServiceTest {

    private static final Long RESTAURANT_ID = 1L;
    public static final Restaurant RESTAURANT = new Restaurant();
    private static final String RESTAURANT_NAME = "Burger";
    private static final String RESTAURANT_DESCR = "Todo tipo de hamburguesa";
    private static final String RESTAURANT_ADDRESS = "Av. Galindo";
    private static final String RESTAURANT_IMAGE = "www.image.com";

    public static final List<Turn> TURNLIST = new ArrayList<>();
    public static final List<Board> BOARD_LIST = new ArrayList<>();
    public static final List<Reservation> RESERVATION_LIST = new ArrayList<>();

    @Mock
    RestaurantRepository restaurantRepository;

    @InjectMocks
    RestaurantServiceImpl restaurantServiceImpl;

    @Before
    public void init() throws BookingException {
        MockitoAnnotations.initMocks(this);

        RESTAURANT.setName(RESTAURANT_NAME);
        RESTAURANT.setDescription(RESTAURANT_DESCR);
        RESTAURANT.setAddress(RESTAURANT_ADDRESS);
        RESTAURANT.setId(RESTAURANT_ID);
        RESTAURANT.setImage(RESTAURANT_IMAGE);
        RESTAURANT.setTurns(TURNLIST);
        RESTAURANT.setBoards(BOARD_LIST);
        RESTAURANT.setReservations(RESERVATION_LIST);
    }

    @Test
    public void getRestaurantByIdTest() throws BookingException {
        Mockito.when(restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.of(RESTAURANT));
        restaurantServiceImpl.getRestaurantById(RESTAURANT_ID);
    }

    @Test(expected = BookingException.class)
    public void getRestaurantByIdTestError() throws BookingException {
        Mockito.when(restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.empty());
        restaurantServiceImpl.getRestaurantById(RESTAURANT_ID);
    }

    @Test
    public void getRestaurantsTest() throws BookingException {
        final Restaurant restaurant =new  Restaurant();
        Mockito.when(restaurantRepository.findAll()).thenReturn(Arrays.asList(restaurant));
        final List<RestaurantRest> response = restaurantServiceImpl.getRestaurants();
        assertNotNull(response);
        assertFalse(response.isEmpty());
        assertEquals(response.size(), 1);

    }

}
